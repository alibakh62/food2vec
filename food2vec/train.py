import os
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')

import time
from glob import glob
from datetime import datetime

from .utils import *
from .dataprep import DataLoader

from mxnet import gluon, np, npx 
from mxnet.contrib import text 
npx.set_np()

import yaml
yml_path = glob(os.path.join('..', 'food2vec', '*.yml'))[0]
with open(yml_path, "rb") as yaml_to_read:
    yml = yaml.safe_load(yaml_to_read)


# Constants
filepath = os.path.join(yml['base_dir'], yml['data_dir'], yml['train_data'])
label_colname = yml['label_col']
ingrd_colname = yml['ingrd_col']
batch_size = yml['batch_size']
max_window_size = yml['max_window_size']
num_noise_words = yml['num_noise_words']


class Food2Vec:

    def __init__(self, pretrained_model):
        pass

    def load_glove(self, filename):
        return text.embedding.create('glove', pretrained_file_name=filename)

    def load_bert(self, filename):
        pass

    def load_gensim(self, filename):
        pass

    def load_w2v(self, filename):
        pass

    