import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import re
import numpy as np
import pandas as pd
import collections
import random
import time
import math
from glob import glob
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

from mxnet import autograd, context, gluon
from mxnet import autograd, gluon, np, npx 
from mxnet.gluon import nn
npx.set_np()

# from .helpers import RandomGenerator, Plot, Timer


import yaml
yml_path = glob(os.path.join('..', 'food2vec', '*.yml'))[0]
with open(yml_path, "rb") as yaml_to_read:
    yml = yaml.safe_load(yaml_to_read)


class Vocab:

    def __init__(self, sentences, min_freq=0, reserved_tokens=None):
        if reserved_tokens is None:
            reserved_tokens = []
        # Sort according to frequencies
        counter = W2VUtils.count_corpus(sentences)
        self.token_freqs = sorted(counter.items(), key=lambda x: x[0])
        self.token_freqs.sort(key=lambda x: x[1], reverse=True)
        self.unk, uniq_tokens = 0, ['<unk>'] + reserved_tokens
        uniq_tokens += [token for token, freq in self.token_freqs
                        if freq >= min_freq and token not in uniq_tokens]
        self.idx_to_token, self.token_to_idx = [], dict()
        for token in uniq_tokens:
            self.idx_to_token.append(token)
            self.token_to_idx[token] = len(self.idx_to_token) - 1

    def __len__(self):
        return len(self.idx_to_token)

    def __getitem__(self, tokens):
        if not isinstance(tokens, (list, tuple)):
            return self.token_to_idx.get(tokens, self.unk)
        return [self.__getitem__(token) for token in tokens]

    def to_tokens(self, indices):
        if not isinstance(indices, (list, tuple)):
            return self.idx_to_token[indices]
        return [self.idx_to_token[index] for index in indices]


class W2VUtils:


    @classmethod
    def remove_stopwords(cls, sentence):
        stop_words = set(stopwords.words('english'))
        return ' '.join(w for w in sentence.split() if w not in stop_words) 

    @classmethod
    def preprocess(cls, input_texts, filter=[], topn=100, remove_space=True):
        from collections import Counter
        texts = []
        for text in input_texts:
            tmp0 = (re.sub("[\(\[].*?[\)\]]", "", text) \
                      .replace(", ", ",") \
                      .replace(",  ", ",") \
                      .replace("  ,", ",") \
                      .replace(" ,", ",") \
                      .replace(" , ", ",") \
                      .lower())
            tmp1 = ','.join([cls.remove_stopwords(x) for x in tmp0.split(',')])  # remove stop words
            lemmatizer = WordNetLemmatizer()
            tmp2 = [lemmatizer.lemmatize(i) for i in tmp1.split(',')]  # lemmatize
            ingredients_cleaned = ','.join(tmp2)
            if remove_space:  #TODO: should we remove space?
                texts.append(re.sub('[^a-zA-Z,]+', '', ingredients_cleaned))  
            else:
                texts.append(re.sub('[^a-zA-Z, ]+', '', ingredients_cleaned))
        ingrd_ = [i for text in texts for i in text.split(',')]
        if filter:
            ingrd = [i for i in ingrd if i not in filter]
        else:
            ingrd = ingrd_
        c = Counter(ingrd).most_common(topn)
        topn_ingrd = [i[0] for i in c]
        return texts, topn_ingrd  

    @classmethod
    def get_sentences(cls, raw_texts):
        texts, _ = cls.preprocess(raw_texts)
        raw_text = ' \n'.join(texts)
        return [line.split() for line in raw_text.split('\n')]

    @staticmethod
    def count_corpus(sentences):
        # Flatten a list of token lists into a list of tokens
        tokens = [tk for line in sentences for tk in line]
        return collections.Counter(tokens)

    @classmethod
    def subsampling(cls, sentences, vocab):
        # Map low frequency words into <unk>
        sentences_ = [[vocab.idx_to_token[vocab[tk]] for tk in line]
                    for line in sentences]
        # Count the frequency for each word
        counter = cls.count_corpus(sentences_)
        num_tokens = sum(counter.values())
        # Return True if to keep this token during subsampling
        def keep(token):
            return(random.uniform(0, 1) <
                math.sqrt(1e-4 / counter[token] * num_tokens))
        # subsampling
        return [[tk for tk in line if keep(tk)] for line in sentences_]

    @classmethod
    def get_centers_and_contexts(cls, corpus, max_window_size):
        centers, contexts = [], []
        for line in corpus:
            # Each sentence needs at least 2 words to form a
            # "central target word - context word" pair
            if len(line) < 2:
                continue
            centers += line
            for i in range(len(line)):  # Context window centered at i
                window_size = random.randint(1, max_window_size)
                indices = list(range(max(0, i - window_size),
                                    min(len(line), i + 1 + window_size)))
                # Exclude the central target word from the context words
                indices.remove(i)
                contexts.append([line[idx] for idx in indices])
        return centers, contexts

    @classmethod
    def get_negatives(cls, all_contexts, sentences, num_noise_words):
        counter = cls.count_corpus(sentences)
        sampling_weights = [counter[i]**0.75 for i in range(len(counter))]
        all_negatives, generator = [], RandomGenerator(sampling_weights)
        for contexts in all_contexts:
            negatives = []
            while len(negatives) < len(contexts) * num_noise_words:
                neg = generator.draw()
                # Noise words cannot be context words
                if neg not in contexts:
                    negatives.append(neg)
            all_negatives.append(negatives)
        return all_negatives

    @staticmethod
    def compare_counts(token, all_sentences, subsampled_sentences):
        try:
            before = sum([line.count(token) for line in all_sentences])
            after = sum([line.count(token) for line in subsampled_sentences])
        except Exception as e:
            print(f"Need to subsample first! {e}")
        return f"# of token {token}: before={before}, after={after}"

    @staticmethod
    def get_corpus(vocab, sentences):
        return [vocab[line] for line in sentences]


class TrainUtils:

    lr = yml['learning_rate']
    batch_size = yml['batch_size']
    num_epochs = yml['num_epochs']
    pretrained_models = {
        'w2v': 'modelname',
        'glove': 'modelname',
        'bert': 'modelname'
    }
    npx = None
    net = None
    
    @classmethod
    def set_npx(cls, npx):
        cls.npx = npx

    @classmethod
    def try_gpu(cls, i=0):
        """
        In case GPU is available
        Return gpu(i) if exists, otherwise return cpu().
        """
        return cls.npx.gpu(i) if cls.npx.num_gpus() >= i + 1 else cls.npx.cpu()

    @classmethod
    def skip_gram(cls, center, contexts_and_negatives, embed_v, embed_u):
        v = embed_v(center) 
        u = embed_u(contexts_and_negatives) 
        pred = cls.npx.batch_dot(v, u.swapaxes(1, 2)) 
        return pred

    @classmethod
    def train(cls, net, data_iter, loss):
        ctx = cls.try_gpu()
        cls.net = net
        cls.net.initialize(ctx=ctx, force_reinit=True)
        trainer = gluon.Trainer(cls.net.collect_params(), 
                                'adam', 
                                {'learning_rate': cls.lr})
        animator = Animator(xlabel='epoch', 
                            ylabel='loss',
                            xlim=[0, cls.num_epochs])
        for epoch in range(cls.num_epochs):
            timer = Timer()
            metric = Accumulator(2)  # loss_sum, num_tokens
            for i, batch in enumerate(data_iter):
                center, context_negative, mask, label = [data.as_in_ctx(ctx) for data in batch]
                with autograd.record():
                    pred = cls.skip_gram(center, context_negative, cls.net[0], cls.net[1])
                    l = (loss(pred.reshape(label.shape), label, mask) / mask.sum(axis=1) * mask.shape[1])
                l.backward()
                trainer.step(cls.batch_size)
                metric.add(l.sum(), l.size)
                if (i+1) % 50 == 0:
                    animator.add(epoch+(i+1)/len(data_iter),
                                (metric[0]/metric[1],))
        print(f"loss {metric[0]/metric[1]}, {metric[1]/timer.stop()} tokens/sec on {ctx}")

    @classmethod
    def save_model(cls, modelpath):
        cls.net.save(modelpath)

    @classmethod
    def get_similar_tokens(cls, query_token, k, vocab):
        import numpy as np
        # if len(embed) == 0:
        embed = cls.net[0]
        W = embed.weight.data()
        x = W[vocab[query_token]]
        # Compute the cosine similarity. Add 1e-9 for numerical stability
        cos = np.dot(W, x) / np.sqrt(np.sum(W * W, axis=1) * np.sum(x * x) + 1e-9)
        topk = cls.npx.topk(cos, k=k+1, ret_typ='indices').asnumpy().astype('int32')
        for i in topk[1:]: # Remove the input words
            print(f"cosine sim={cos[i]}: {vocab.idx_to_token[i]}")

    @classmethod
    def knn(cls, W, x, k):
        # The added 1e-9 is for numerical stability
        import numpy as np
        cos = np.dot(W, x.reshape(-1,)) / (
              np.sqrt(np.sum(W * W, axis=1) + 1e-9) * np.sqrt((x * x).sum()))
        topk = cls.npx.topk(cos, k=k, ret_typ='indices')
        return topk, [cos[int(i)] for i in topk]

    @classmethod
    def get_similar_tokens_knn(cls, query_token, k, embed):
        topk, cos = cls.knn(embed.idx_to_vec,
                            embed.get_vecs_by_tokens([query_token]), k+1)
        for i, c in zip(topk[1:], cos[1:]): # Remove input words
            print(f"cosine sim={c}: {embed.idx_to_token[int(i)]}")

    @staticmethod
    def load_pretrained(modelname=None):
        import numpy as np
        embeddings = {}
        f = open(os.path.join('..', 'models', 'glove.6B.100d.txt'))
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings[word] = coefs
        f.close()
        return embeddings

    @classmethod
    def _load_pretrained(cls, modelname):
        model = cls.pretrained_models[modelname]
        import numpy as np
        embeddings = {}
        f = open(os.path.join('..', 'models', model))
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings[word] = coefs
        f.close()
        return embeddings

    @staticmethod
    def get_food_vectors(texts, embeddings, embed_size=100):
        '''
            Averages ingredients vectors to get food vector
        '''
        import numpy as np
        texts_, _ = W2VUtils.preprocess(texts)
        food_vec = []
        for item in texts_:
            cnt = 1
            foodvec = np.zeros(embed_size)  # embed size
            ingrds = item.split(',')
            for ingrd in ingrds:
                ingrd_ = ingrd.split(' ')
                for e in ingrd_:
                    try:
                        foodvec += embeddings[e]
                        cnt += 1
                    except KeyError:
                        pass
            foodvec /= cnt
            food_vec.append(foodvec)
        return food_vec

    @staticmethod
    def calc_cosine_similarity(vec1, vec2):
        v1 = vec1[:,np.newaxis]
        v2 = vec2[:,np.newaxis]
        import math
        def dot_product(v1, v2):
            return sum(map(lambda x: x[0] * x[1], zip(v1, v2)))
        prod = dot_product(v1, v2)
        len1 = math.sqrt(dot_product(v1, v1))
        len2 = math.sqrt(dot_product(v2, v2))
        return prod / (len1 * len2)

    @staticmethod
    def get_food_index(labels, foodname):
        return labels.index(foodname)

    @staticmethod
    def find_similar_food(query_token, food_vectors, food_labels, topn=5):
        try:
            query_index = cls.get_food_index(query_token)
        except KeyError:
            print("Food token not found!")
        query_vec = food_vectors[query_index]
        similarity_scores = {}
        for i, food in enumerate(food_vectors):
            if i != query_index:
                similarity_scores[i] = cls.calc_cosine_similarity(query_vec, food)[0]
        # sort by similarity (ascending)
        sorted_scores = {k: v for k, v in sorted(similarity_scores.items(), key=lambda item: item[1])}
        idx_top = list(sorted_scores)[-topn:]
        idx_top_ = list(sorted_scores)[:topn]
        most_similar, least_similar = [], []
        for idx in idx_top:
            most_similar.append(food_labels[idx])
        for idx in idx_top_:
            least_similar.append(food_labels[idx])
        return sorted_scores, most_similar, least_similar

    @staticmethod
    def tokenize(lines, token='word'):
        """Split sentences into word or char tokens."""
        if token == 'word':
            return [line.split(' ') for line in lines]
        elif token == 'char':
            return [list(line) for line in lines]
        else:
            print('ERROR: unknown token type '+token)

    @staticmethod
    def truncate_pad(line, num_steps, padding_token):
        """
           padding to the same length
        """
        if len(line) > num_steps:
            return line[:num_steps]  # Trim
        return line + [padding_token] * (num_steps - len(line))  # Pad

    @staticmethod
    def load_array(data_arrays, batch_size, is_train=True):
        """Construct a Gluon data loader"""
        dataset = gluon.data.ArrayDataset(*data_arrays)
        return gluon.data.DataLoader(dataset, batch_size, shuffle=is_train)

    @classmethod
    def try_all_gpus(cls):
        """Return all available GPUs, or [cpu(),] if no GPU exists."""
        ctxes = [cls.npx.gpu(i) for i in range(cls.npx.num_gpus())]
        return ctxes if ctxes else [cls.npx.cpu()]

    @staticmethod
    def split_batch(X, y, ctx_list):
        """Split X and y into multiple devices specified by ctx."""
        assert X.shape[0] == y.shape[0]
        return (gluon.utils.split_and_load(X, ctx_list),
                gluon.utils.split_and_load(y, ctx_list))

    @classmethod
    def train_batch(cls, net, features, labels, loss, trainer, ctx_list, split_f=split_batch):
        X_shards, y_shards = split_f(features, labels, ctx_list)
        with autograd.record():
            pred_shards = [net(X_shard) for X_shard in X_shards]
            ls = [loss(pred_shard, y_shard) for pred_shard, y_shard
                in zip(pred_shards, y_shards)]
        for l in ls:
            l.backward()
        # The True flag allows parameters with stale gradients, which is useful
        # later (e.g., in fine-tuning BERT)
        trainer.step(labels.shape[0], ignore_stale_grad=True)
        train_loss_sum = sum([float(l.sum()) for l in ls])
        train_acc_sum = sum(cls.accuracy(pred_shard, y_shard)
                            for pred_shard, y_shard in zip(pred_shards, y_shards))
        return train_loss_sum, train_acc_sum

    @staticmethod
    def accuracy(y_hat, y):
        if y_hat.shape[1] > 1:
            return float((y_hat.argmax(axis=1).astype('float32') == y.astype(
                'float32')).sum())
        else:
            return float((y_hat.astype('int32') == y.astype('int32')).sum())

    @classmethod
    def evaluate_accuracy_gpus(cls, net, data_iter, split_f=split_batch):
        # Query the list of devices
        ctx = list(net.collect_params().values())[0].list_ctx()
        metric = Accumulator(2)  # num_corrected_examples, num_examples
        for features, labels in data_iter:
            X_shards, y_shards = split_f(features, labels, ctx)
            # Run in parallel
            pred_shards = [net(X_shard) for X_shard in X_shards]
            metric.add(sum(float(cls.accuracy(pred_shard, y_shard)) for
                        pred_shard, y_shard in zip(
                            pred_shards, y_shards)), labels.size)
        return metric[0] / metric[1]

    @classmethod
    def train_classifier(cls, net, train_iter, test_iter, loss, trainer, num_epochs,
                         split_f=split_batch):
        ctx_list = cls.try_all_gpus()
        num_batches, timer = len(train_iter), Timer()
        animator = Animator(xlabel='epoch', xlim=[0, num_epochs], ylim=[0, 1],
                                legend=['train loss', 'train acc', 'test acc'])
        for epoch in range(num_epochs):
            # Store training_loss, training_accuracy, num_examples, num_features
            metric = Accumulator(4)
            for i, (features, labels) in enumerate(train_iter):
                timer.start()
                l, acc = cls.train_batch(
                    net, features, labels, loss, trainer, ctx_list, split_f)
                metric.add(l, acc, labels.shape[0], labels.size)
                timer.stop()
                if (i + 1) % (num_batches // 5) == 0:
                    animator.add(epoch + i / num_batches,
                                (metric[0] / metric[2], metric[1] / metric[3],
                                None))
            test_acc = cls.evaluate_accuracy_gpus(net, test_iter, split_f)
            animator.add(epoch + 1, (None, None, test_acc))
        print('loss %.3f, train acc %.3f, test acc %.3f' % (
            metric[0] / metric[2], metric[1] / metric[3], test_acc))
        print('%.1f examples/sec on %s' % (
            metric[2] * num_epochs / timer.sum(), ctx_list))

    @staticmethod
    def predict_food(net, vocab, sentence):
        sentence = np.array(vocab[sentence.split()], ctx=try_gpu())
        label = np.argmax(net(sentence.reshape(1, -1)), axis=1)
        return label, le.inverse_transform([int(label[0])])



