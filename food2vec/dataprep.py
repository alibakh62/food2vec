import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import re
import numpy as np
import pandas as pd
from glob import glob

from utils.dataprep_utils import Vocab, W2VUtils

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
stopwords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()

import yaml
yml_path = glob(os.path.join('..', 'food2vec', '*.yml'))[0]
with open(yml_path, "rb") as yaml_to_read:
    yml = yaml.safe_load(yaml_to_read)


class DataLoader:

    def __init__(self):
        pass

    def get_texts_labels(self, filepath, label_colname, ingrd_colname):
        df = pd.read_csv(filepath)
        df = df.loc[df[label_colname].notnull(),]
        df = df.loc[df[ingrd_colname].notnull(),]
        texts = list(df[ingrd_colname].values)
        labels = list(df[label_colname].values)
        return df, texts, labels

    def get_dataloader_workers(self, num_workers=4):
        # 0 means no additional process is used to speed up the reading of data.
        if sys.platform.startswith('win'):
            return 0
        else:
            return num_workers

    def batchify(self, data):
        max_len = max(len(c) + len(n) for _, c, n in data)
        centers, contexts_negatives, masks, labels = [], [], [], []
        for center, context, negative in data:
            cur_len = len(context) + len(negative)
            centers += [center]
            contexts_negatives += [context + negative + [0] * (max_len - cur_len)]
            masks += [[1] * cur_len + [0] * (max_len - cur_len)]
            labels += [[1] * len(context) + [0] * (max_len - len(context))]
        return (np.array(centers).reshape(-1, 1), np.array(contexts_negatives),
                np.array(masks), np.array(labels))

    def dataloader(self, texts, batch_size, max_window_size, num_noise_words):
        num_workers = self.get_dataloader_workers()
        sentences = W2VUtils.get_sentences(texts)
        vocab = Vocab(sentences, min_freq=10)
        subsampled = W2VUtils.subsampling(sentences, vocab)
        corpus = W2VUtils.get_corpus(vocab, subsampled)
        all_centers, all_contexts = W2VUtils.get_centers_and_contexts(corpus, max_window_size)
        all_negatives = W2VUtils.get_negatives(all_contexts, sentences, num_noise_words)
        dataset = gluon.data.ArrayDataset(all_centers, all_contexts, all_negatives)
        data_iter = gluon.data.DataLoader(dataset, batch_size, shuffle=True,
                                          batchify_fn=self.batchify,
                                          num_workers=num_workers)
        return data_iter, vocab