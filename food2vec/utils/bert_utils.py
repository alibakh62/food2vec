import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import numpy as np
import unicodedata
import tensorflow as tf
from tensorflow import keras
import tensorflow.keras.backend as K

from .layers.inputs import get_inputs
from .layers.embedding import get_embedding, TokenEmbedding, EmbeddingSimilarity, LayerNormalization
from .layers.mask import Masked
from .layers.extract import Extract
from .layers.task_embed import TaskEmbedding


# Special tokens
TOKEN_PAD = ''  # Token for padding
TOKEN_UNK = '[UNK]'  # Token for unknown words
TOKEN_CLS = '[CLS]'  # Token for classification
TOKEN_SEP = '[SEP]'  # Token for separation
TOKEN_MASK = '[MASK]'  # Token for masking


class LoadBert:

    def __init__(self):
        pass

    def gelu(self, x):
        from tensorflow.math import erf, sqrt
        return 0.5 * x * (1.0 + erf(x / sqrt(2.0)))
    
    def get_model(self, 
                 token_num,
                 pos_num=512,
                 seq_len=512,
                 embed_dim=768,
                 transformer_num=12,
                 head_num=12,
                 feed_forward_dim=3072,
                 dropout_rate=0.1,
                 attention_activation=None,
                 feed_forward_activation='gelu',
                 training=True,
                 trainable=None,
                 output_layer_num=1,
                 use_task_embed=False,
                 task_num=10,
                 use_adapter=False,
                 adapter_units=None):
        """Get BERT model.
        See: https://arxiv.org/pdf/1810.04805.pdf
        :param token_num: Number of tokens.
        :param pos_num: Maximum position.
        :param seq_len: Maximum length of the input sequence or None.
        :param embed_dim: Dimensions of embeddings.
        :param transformer_num: Number of transformers.
        :param head_num: Number of heads in multi-head attention in each transformer.
        :param feed_forward_dim: Dimension of the feed forward layer in each transformer.
        :param dropout_rate: Dropout rate.
        :param attention_activation: Activation for attention layers.
        :param feed_forward_activation: Activation for feed-forward layers.
        :param training: A built model with MLM and NSP outputs will be returned if it is `True`,
                        otherwise the input layers and the last feature extraction layer will be returned.
        :param trainable: Whether the model is trainable.
        :param output_layer_num: The number of layers whose outputs will be concatenated as a single output.
                                Only available when `training` is `False`.
        :param use_task_embed: Whether to add task embeddings to existed embeddings.
        :param task_num: The number of tasks.
        :param use_adapter: Whether to use feed-forward adapters before each residual connections.
        :param adapter_units: The dimension of the first transformation in feed-forward adapter.
        :return: The built model.
        """
        if attention_activation == 'gelu':
            attention_activation = self.gelu
        if feed_forward_activation == 'gelu':
            feed_forward_activation = self.gelu
        if trainable is None:
            trainable = training
        if adapter_units is None:
            adapter_units = max(1, embed_dim // 100)

        def _trainable(_layer):
            if isinstance(trainable, (list, tuple, set)):
                for prefix in trainable:
                    if _layer.name.startswith(prefix):
                        return True
                return False
            return trainable

        inputs = get_inputs(seq_len=seq_len)
        embed_layer, embed_weights = get_embedding(
            inputs,
            token_num=token_num,
            embed_dim=embed_dim,
            pos_num=pos_num,
            dropout_rate=dropout_rate,
        )
        if use_task_embed:
            task_input = keras.layers.Input(
                shape=(1,),
                name='Input-Task',
            )
            embed_layer = TaskEmbedding(
                input_dim=task_num,
                output_dim=embed_dim,
                mask_zero=False,
                name='Embedding-Task',
            )([embed_layer, task_input])
            inputs = inputs[:2] + [task_input, inputs[-1]]
        if dropout_rate > 0.0:
            dropout_layer = keras.layers.Dropout(
                rate=dropout_rate,
                name='Embedding-Dropout',
            )(embed_layer)
        else:
            dropout_layer = embed_layer
        embed_layer = LayerNormalization(
            trainable=trainable,
            name='Embedding-Norm',
        )(dropout_layer)
        transformed = get_encoders(
            encoder_num=transformer_num,
            input_layer=embed_layer,
            head_num=head_num,
            hidden_dim=feed_forward_dim,
            attention_activation=attention_activation,
            feed_forward_activation=feed_forward_activation,
            dropout_rate=dropout_rate,
            use_adapter=use_adapter,
            adapter_units=adapter_units,
            adapter_activation=self.gelu,
        )
        if training:
            mlm_dense_layer = keras.layers.Dense(
                units=embed_dim,
                activation=feed_forward_activation,
                name='MLM-Dense',
            )(transformed)
            mlm_norm_layer = LayerNormalization(name='MLM-Norm')(mlm_dense_layer)
            mlm_pred_layer = EmbeddingSimilarity(name='MLM-Sim')([mlm_norm_layer, embed_weights])
            masked_layer = Masked(name='MLM')([mlm_pred_layer, inputs[-1]])
            extract_layer = Extract(index=0, name='Extract')(transformed)
            nsp_dense_layer = keras.layers.Dense(
                units=embed_dim,
                activation='tanh',
                name='NSP-Dense',
            )(extract_layer)
            nsp_pred_layer = keras.layers.Dense(
                units=2,
                activation='softmax',
                name='NSP',
            )(nsp_dense_layer)
            model = keras.models.Model(inputs=inputs, outputs=[masked_layer, nsp_pred_layer])
            for layer in model.layers:
                layer.trainable = _trainable(layer)
            return model
        else:
            if use_task_embed:
                inputs = inputs[:3]
            else:
                inputs = inputs[:2]
            model = keras.models.Model(inputs=inputs, outputs=transformed)
            for layer in model.layers:
                layer.trainable = _trainable(layer)
            if isinstance(output_layer_num, int):
                output_layer_num = min(output_layer_num, transformer_num)
                output_layer_num = [-i for i in range(1, output_layer_num + 1)]
            outputs = []
            for layer_index in output_layer_num:
                if layer_index < 0:
                    layer_index = transformer_num + layer_index
                layer_index += 1
                layer = model.get_layer(name='Encoder-{}-FeedForward-Norm'.format(layer_index))
                outputs.append(layer.output)
            if len(outputs) > 1:
                transformed = keras.layers.Concatenate(name='Encoder-Output')(list(reversed(outputs)))
            else:
                transformed = outputs[0]
            return inputs, transformed

    def checkpoint_loader(self, checkpoint_file):
        def _loader(name):
            return tf.train.load_variable(checkpoint_file, name)
        return _loader


    def build_model_from_config(self, 
                                config_file,
                                training=False,
                                trainable=None,
                                output_layer_num=1,
                                seq_len=int(1e9),
                                **kwargs):
        """Build the model from config file.
        :param config_file: The path to the JSON configuration file.
        :param training: If training, the whole model will be returned.
                        Otherwise, the MLM and NSP parts will be ignored.
        :param trainable: Whether the model is trainable.
        :param output_layer_num: The number of layers whose outputs will be concatenated as a single output.
                                Only available when `training` is `False`.
        :param seq_len: If it is not None and it is shorter than the value in the config file, the weights in
                        position embeddings will be sliced to fit the new length.
        :return: model and config
        """
        with open(config_file, 'r') as reader:
            config = json.loads(reader.read())
        if seq_len is not None:
            config['max_position_embeddings'] = seq_len = min(seq_len, config['max_position_embeddings'])
        if trainable is None:
            trainable = training
        model = self.get_model(
            token_num=config['vocab_size'],
            pos_num=config['max_position_embeddings'],
            seq_len=seq_len,
            embed_dim=config['hidden_size'],
            transformer_num=config['num_hidden_layers'],
            head_num=config['num_attention_heads'],
            feed_forward_dim=config['intermediate_size'],
            feed_forward_activation=config['hidden_act'],
            training=training,
            trainable=trainable,
            output_layer_num=output_layer_num,
            **kwargs)
        if not training:
            inputs, outputs = model
            model = keras.models.Model(inputs=inputs, outputs=outputs)
        return model, config


    def load_model_weights_from_checkpoint(self,
                                           model,
                                           config,
                                           checkpoint_file,
                                           training=False):
        """Load trained official model from checkpoint.
        :param model: Built keras model.
        :param config: Loaded configuration file.
        :param checkpoint_file: The path to the checkpoint files, should end with '.ckpt'.
        :param training: If training, the whole model will be returned.
                        Otherwise, the MLM and NSP parts will be ignored.
        """
        loader = self.checkpoint_loader(checkpoint_file)

        model.get_layer(name='Embedding-Token').set_weights([
            loader('bert/embeddings/word_embeddings'),
        ])
        model.get_layer(name='Embedding-Position').set_weights([
            loader('bert/embeddings/position_embeddings')[:config['max_position_embeddings'], :],
        ])
        model.get_layer(name='Embedding-Segment').set_weights([
            loader('bert/embeddings/token_type_embeddings'),
        ])
        model.get_layer(name='Embedding-Norm').set_weights([
            loader('bert/embeddings/LayerNorm/gamma'),
            loader('bert/embeddings/LayerNorm/beta'),
        ])
        for i in range(config['num_hidden_layers']):
            try:
                model.get_layer(name='Encoder-%d-MultiHeadSelfAttention' % (i + 1))
            except ValueError:
                continue
            model.get_layer(name='Encoder-%d-MultiHeadSelfAttention' % (i + 1)).set_weights([
                loader('bert/encoder/layer_%d/attention/self/query/kernel' % i),
                loader('bert/encoder/layer_%d/attention/self/query/bias' % i),
                loader('bert/encoder/layer_%d/attention/self/key/kernel' % i),
                loader('bert/encoder/layer_%d/attention/self/key/bias' % i),
                loader('bert/encoder/layer_%d/attention/self/value/kernel' % i),
                loader('bert/encoder/layer_%d/attention/self/value/bias' % i),
                loader('bert/encoder/layer_%d/attention/output/dense/kernel' % i),
                loader('bert/encoder/layer_%d/attention/output/dense/bias' % i),
            ])
            model.get_layer(name='Encoder-%d-MultiHeadSelfAttention-Norm' % (i + 1)).set_weights([
                loader('bert/encoder/layer_%d/attention/output/LayerNorm/gamma' % i),
                loader('bert/encoder/layer_%d/attention/output/LayerNorm/beta' % i),
            ])
            model.get_layer(name='Encoder-%d-FeedForward' % (i + 1)).set_weights([
                loader('bert/encoder/layer_%d/intermediate/dense/kernel' % i),
                loader('bert/encoder/layer_%d/intermediate/dense/bias' % i),
                loader('bert/encoder/layer_%d/output/dense/kernel' % i),
                loader('bert/encoder/layer_%d/output/dense/bias' % i),
            ])
            model.get_layer(name='Encoder-%d-FeedForward-Norm' % (i + 1)).set_weights([
                loader('bert/encoder/layer_%d/output/LayerNorm/gamma' % i),
                loader('bert/encoder/layer_%d/output/LayerNorm/beta' % i),
            ])
        if training:
            model.get_layer(name='MLM-Dense').set_weights([
                loader('cls/predictions/transform/dense/kernel'),
                loader('cls/predictions/transform/dense/bias'),
            ])
            model.get_layer(name='MLM-Norm').set_weights([
                loader('cls/predictions/transform/LayerNorm/gamma'),
                loader('cls/predictions/transform/LayerNorm/beta'),
            ])
            model.get_layer(name='MLM-Sim').set_weights([
                loader('cls/predictions/output_bias'),
            ])
            model.get_layer(name='NSP-Dense').set_weights([
                loader('bert/pooler/dense/kernel'),
                loader('bert/pooler/dense/bias'),
            ])
            model.get_layer(name='NSP').set_weights([
                np.transpose(loader('cls/seq_relationship/output_weights')),
                loader('cls/seq_relationship/output_bias'),
            ])


    def load_trained_model_from_checkpoint(self, 
                                           config_file,
                                           checkpoint_file,
                                           training=False,
                                           trainable=None,
                                           output_layer_num=1,
                                           seq_len=int(1e9),
                                           **kwargs):
        """Load trained official model from checkpoint.
        :param config_file: The path to the JSON configuration file.
        :param checkpoint_file: The path to the checkpoint files, should end with '.ckpt'.
        :param training: If training, the whole model will be returned.
                        Otherwise, the MLM and NSP parts will be ignored.
        :param trainable: Whether the model is trainable. The default value is the same with `training`.
        :param output_layer_num: The number of layers whose outputs will be concatenated as a single output.
                                Only available when `training` is `False`.
        :param seq_len: If it is not None and it is shorter than the value in the config file, the weights in
                        position embeddings will be sliced to fit the new length.
        :return: model
        """
        model, config = self.build_model_from_config(
            config_file,
            training=training,
            trainable=trainable,
            output_layer_num=output_layer_num,
            seq_len=seq_len,
            **kwargs)
        self.load_model_weights_from_checkpoint(model, config, checkpoint_file, training=training)
        return model


    def load_vocabulary(self, vocab_path):
        token_dict = {}
        with codecs.open(vocab_path, 'r', 'utf8') as reader:
            for line in reader:
                token = line.strip()
                token_dict[token] = len(token_dict)
        return token_dict    


class Tokenizer:

    def __init__(self,
                 token_dict,
                 token_cls=TOKEN_CLS,
                 token_sep=TOKEN_SEP,
                 token_unk=TOKEN_UNK,
                 pad_index=0,
                 cased=False):
        """Initialize tokenizer.
        :param token_dict: A dict maps tokens to indices.
        :param token_cls: The token represents classification.
        :param token_sep: The token represents separator.
        :param token_unk: The token represents unknown token.
        :param pad_index: The index to pad.
        :param cased: Whether to keep the case.
        """
        self._token_dict = token_dict
        self._token_dict_inv = {v: k for k, v in token_dict.items()}
        self._token_cls = token_cls
        self._token_sep = token_sep
        self._token_unk = token_unk
        self._pad_index = pad_index
        self._cased = cased

    @staticmethod
    def _truncate(first_tokens, second_tokens=None, max_len=None):
        if max_len is None:
            return

        if second_tokens is not None:
            while True:
                total_len = len(first_tokens) + len(second_tokens)
                if total_len <= max_len - 3:  # 3 for [CLS] .. tokens_a .. [SEP] .. tokens_b [SEP]
                    break
                if len(first_tokens) > len(second_tokens):
                    first_tokens.pop()
                else:
                    second_tokens.pop()
        else:
            del first_tokens[max_len - 2:]  # 2 for [CLS] .. tokens .. [SEP]

    def _pack(self, first_tokens, second_tokens=None):
        first_packed_tokens = [self._token_cls] + first_tokens + [self._token_sep]
        if second_tokens is not None:
            second_packed_tokens = second_tokens + [self._token_sep]
            return first_packed_tokens + second_packed_tokens, len(first_packed_tokens), len(second_packed_tokens)
        else:
            return first_packed_tokens, len(first_packed_tokens), 0

    def _convert_tokens_to_ids(self, tokens):
        unk_id = self._token_dict.get(self._token_unk)
        return [self._token_dict.get(token, unk_id) for token in tokens]

    def tokenize(self, first, second=None):
        """Split text to tokens.
        :param first: First text.
        :param second: Second text.
        :return: A list of strings.
        """
        first_tokens = self._tokenize(first)
        second_tokens = self._tokenize(second) if second is not None else None
        tokens, _, _ = self._pack(first_tokens, second_tokens)
        return tokens

    def encode(self, first, second=None, max_len=None):
        first_tokens = self._tokenize(first)
        second_tokens = self._tokenize(second) if second is not None else None
        self._truncate(first_tokens, second_tokens, max_len)
        tokens, first_len, second_len = self._pack(first_tokens, second_tokens)

        token_ids = self._convert_tokens_to_ids(tokens)
        segment_ids = [0] * first_len + [1] * second_len

        if max_len is not None:
            pad_len = max_len - first_len - second_len
            token_ids += [self._pad_index] * pad_len
            segment_ids += [0] * pad_len

        return token_ids, segment_ids

    def decode(self, ids):
        sep = ids.index(self._token_dict[self._token_sep])
        try:
            stop = ids.index(self._pad_index)
        except ValueError as e:
            stop = len(ids)
        tokens = [self._token_dict_inv[i] for i in ids]
        first = tokens[1:sep]
        if sep < stop - 1:
            second = tokens[sep + 1:stop - 1]
            return first, second
        return first

    def _tokenize(self, text):
        if not self._cased:
            text = unicodedata.normalize('NFD', text)
            text = ''.join([ch for ch in text if unicodedata.category(ch) != 'Mn'])
            text = text.lower()
        spaced = ''
        for ch in text:
            if self._is_punctuation(ch) or self._is_cjk_character(ch):
                spaced += ' ' + ch + ' '
            elif self._is_space(ch):
                spaced += ' '
            elif ord(ch) == 0 or ord(ch) == 0xfffd or self._is_control(ch):
                continue
            else:
                spaced += ch
        tokens = []
        for word in spaced.strip().split():
            tokens += self._word_piece_tokenize(word)
        return tokens

    def _word_piece_tokenize(self, word):
        if word in self._token_dict:
            return [word]
        tokens = []
        start, stop = 0, 0
        while start < len(word):
            stop = len(word)
            while stop > start:
                sub = word[start:stop]
                if start > 0:
                    sub = '##' + sub
                if sub in self._token_dict:
                    break
                stop -= 1
            if start == stop:
                stop += 1
            tokens.append(sub)
            start = stop
        return tokens

    @staticmethod
    def _is_punctuation(ch):
        code = ord(ch)
        return 33 <= code <= 47 or \
            58 <= code <= 64 or \
            91 <= code <= 96 or \
            123 <= code <= 126 or \
            unicodedata.category(ch).startswith('P')

    @staticmethod
    def _is_cjk_character(ch):
        code = ord(ch)
        return 0x4E00 <= code <= 0x9FFF or \
            0x3400 <= code <= 0x4DBF or \
            0x20000 <= code <= 0x2A6DF or \
            0x2A700 <= code <= 0x2B73F or \
            0x2B740 <= code <= 0x2B81F or \
            0x2B820 <= code <= 0x2CEAF or \
            0xF900 <= code <= 0xFAFF or \
            0x2F800 <= code <= 0x2FA1F

    @staticmethod
    def _is_space(ch):
        return ch == ' ' or ch == '\n' or ch == '\r' or ch == '\t' or \
            unicodedata.category(ch) == 'Zs'

    @staticmethod
    def _is_control(ch):
        return unicodedata.category(ch) in ('Cc', 'Cf')

    @staticmethod
    def rematch(text, tokens, cased=False, unknown_token=TOKEN_UNK):
        """Try to find the indices of tokens in the original text.
        >>> Tokenizer.rematch("All rights reserved.", ["all", "rights", "re", "##ser", "##ved", "."])
        [(0, 3), (4, 10), (11, 13), (13, 16), (16, 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.", ["all", "rights", "re", "##ser", "[UNK]", "."])
        [(0, 3), (4, 10), (11, 13), (13, 16), (16, 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.", ["[UNK]", "rights", "[UNK]", "##ser", "[UNK]", "[UNK]"])
        [(0, 3), (4, 10), (11, 13), (13, 16), (16, 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.", ["[UNK]", "righs", "[UNK]", "ser", "[UNK]", "[UNK]"])
        [(0, 3), (4, 10), (11, 13), (13, 16), (16, 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.",
        ...                  ["[UNK]", "rights", "[UNK]", "[UNK]", "[UNK]", "[UNK]"])  # doctest:+ELLIPSIS
        [(0, 3), (4, 10), (11, ... 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.", ["all rights", "reserved", "."])
        [(0, 10), (11, 19), (19, 20)]
        >>> Tokenizer.rematch("All rights reserved.", ["all rights", "reserved", "."], cased=True)
        [(0, 10), (11, 19), (19, 20)]
        >>> Tokenizer.rematch("#hash tag ##", ["#", "hash", "tag", "##"])
        [(0, 1), (1, 5), (6, 9), (10, 12)]
        >>> Tokenizer.rematch("嘛呢，吃了吗？", ["[UNK]", "呢", "，", "[UNK]", "了", "吗", "？"])
        [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7)]
        >>> Tokenizer.rematch("  吃了吗？    ", ["吃", "了", "吗", "？"])
        [(2, 3), (3, 4), (4, 5), (5, 6)]
        :param text: Original text.
        :param tokens: Decoded list of tokens.
        :param cased: Whether it is cased.
        :param unknown_token: The representation of unknown token.
        :return: A list of tuples represents the start and stop locations in the original text.
        """
        decoded, token_offsets = '', []
        for token in tokens:
            token_offsets.append([len(decoded), 0])
            if token == unknown_token:
                token = '#'
            if not cased:
                token = token.lower()
            if len(token) > 2 and token.startswith('##'):
                token = token[2:]
            elif len(decoded) > 0:
                token = ' ' + token
                token_offsets[-1][0] += 1
            decoded += token
            token_offsets[-1][1] = len(decoded)

        heading = 0
        text = text.rstrip()
        for i in range(len(text)):
            if not Tokenizer._is_space(text[i]):
                break
            heading += 1
        text = text[heading:]
        len_text, len_decode = len(text), len(decoded)
        costs = [[0] * (len_decode + 1) for _ in range(2)]
        paths = [[(-1, -1)] * (len_decode + 1) for _ in range(len_text + 1)]
        curr, prev = 0, 1

        for j in range(len_decode + 1):
            costs[curr][j] = j
        for i in range(1, len_text + 1):
            curr, prev = prev, curr
            costs[curr][0] = i
            ch = text[i - 1]
            if not cased:
                ch = ch.lower()
            for j in range(1, len_decode + 1):
                costs[curr][j] = costs[prev][j - 1]
                paths[i][j] = (i - 1, j - 1)
                if ch != decoded[j - 1]:
                    costs[curr][j] = costs[prev][j - 1]
                    paths[i][j] = (i - 1, j - 1)
                    if costs[prev][j] < costs[curr][j]:
                        costs[curr][j] = costs[prev][j]
                        paths[i][j] = (i - 1, j)
                    if costs[curr][j - 1] < costs[curr][j]:
                        costs[curr][j] = costs[curr][j - 1]
                        paths[i][j] = (i, j - 1)
                    costs[curr][j] += 1

        matches = [0] * (len_decode + 1)
        position = (len_text, len_decode)
        while position != (-1, -1):
            i, j = position
            matches[j] = i
            position = paths[i][j]

        intervals = [[matches[offset[0]], matches[offset[1]]] for offset in token_offsets]
        for i, interval in enumerate(intervals):
            token_a, token_b = text[interval[0]:interval[1]], tokens[i]
            if len(token_b) > 2 and token_b.startswith('##'):
                token_b = token_b[2:]
            if not cased:
                token_a, token_b = token_a.lower(), token_b.lower()
            if token_a == token_b:
                continue
            if i == 0:
                border = 0
            else:
                border = intervals[i - 1][1]
            for j in range(interval[0] - 1, border - 1, -1):
                if Tokenizer._is_space(text[j]):
                    break
                interval[0] -= 1
            if i + 1 == len(intervals):
                border = len_text
            else:
                border = intervals[i + 1][0]
            for j in range(interval[1], border):
                if Tokenizer._is_space(text[j]):
                    break
                interval[1] += 1
        intervals = [(interval[0] + heading, interval[1] + heading) for interval in intervals]
        return intervals