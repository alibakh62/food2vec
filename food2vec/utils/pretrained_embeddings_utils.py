import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import numpy as np

from keras_bert import load_vocabulary, load_trained_model_from_checkpoint, Tokenizer, get_checkpoint_paths
from keras_bert.datasets import get_pretrained, PretrainedList


class LoadEmbeddings:

    def __init__(self, embed_model):
        self.embed_model = embed_model
        self.embeddings = None
        self.paths = None
        try:
            if embed_model == 'bert':
                print("loading BERT embeddings ...")
                self.load_bert()
            elif embed_model == 'glove':
                print("loading GloVe embeddings ...")
                self.load_glove()
            else:
                print("loading Gensim W2V embeddings ...")
                self.load_gensim_w2v()
        except KeyError:
            print("embedding method is not defined!")

    def load_gensim_w2v(self, modelpath=None):
        pass

    def load_glove(self, modelpath=None):
        self.embeddings = {}   #TODO: fix model path
        base_dir = '/Users/abakh005/Documents/seleven/workspace/projects/StoreAI/models'  #os.path.split(os.path.abspath("."))[0]
        f = open(os.path.join(base_dir, 'glove.6B.100d.txt'))
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            self.embeddings[word] = coefs
        f.close()
        return self.embeddings

    def load_bert(self, modelpath=None, seq_len=10):
        if modelpath is None:
            modelpath = get_pretrained(PretrainedList.wwm_uncased_large)
        else:
            pass  #TODO: add custom model fetch
        self.paths = get_checkpoint_paths(modelpath)
        self.embeddings = load_trained_model_from_checkpoint(self.paths.config, 
                                                             self.paths.checkpoint, 
                                                             seq_len=seq_len)
        return self.embeddings, self.paths

    def get_embedding(self, token, max_len=10):
        if self.embed_model == 'bert':
            token_dict = load_vocabulary(self.paths.vocab)
            tokenizer = Tokenizer(token_dict)
            # bert_tokens = tokenizer.tokenize(token)
            indices, segments = tokenizer.encode(first=token, max_len=max_len)
            predicts = self.embeddings.predict([np.array([indices]), np.array([segments])])[0]
            return predicts[1].tolist()
        elif self.embed_model == 'glove':
            return self.embeddings[token]
        elif self.embed_model == 'gensim_w2v':
            return 'gensim not implemented yet! Try bert or glove'
        