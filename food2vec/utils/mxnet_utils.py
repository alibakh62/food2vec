import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import re
import random
import time
import numpy as np
import matplotlib.pyplot as plt
try:
    from IPython import display
    from IPython.display import set_matplotlib_formats
except:
    pass


class Plot:

    def __init__(self, figsize=(10,5)):
        self.figsize = figsize

    def use_svg_display(self):
        """Use the svg format to display a plot in Jupyter."""
        set_matplotlib_formats('svg')

    def set_figsize(self):
        """Set the figure size for matplotlib."""
        self.use_svg_display()
        plt.rcParams['figure.figsize'] = self.figsize

    def set_axes(self, axes, xlabel, ylabel, xlim, ylim, xscale, yscale, legend):
        """Set the axes for matplotlib."""
        axes.set_xlabel(xlabel)
        axes.set_ylabel(ylabel)
        axes.set_xscale(xscale)
        axes.set_yscale(yscale)
        axes.set_xlim(xlim)
        axes.set_ylim(ylim)
        if len(legend) > 0:
            axes.legend(legend)
        axes.grid()


class RandomGenerator:
    """Draw a random int in [0, n] according to n sampling weights."""
    import random

    def __init__(self, sampling_weights):
        self.population = list(range(len(sampling_weights)))
        self.sampling_weights = sampling_weights
        self.candidates = []
        self.i = 0

    def draw(self, k=10000):
        if self.i == len(self.candidates):
            self.candidates = random.choices(self.population, self.sampling_weights, k=k)
            self.i = 0
        self.i += 1
        return self.candidates[self.i-1]


class Animator:
    """Incrementally plot multiple lines."""

    def __init__(self, 
                 xlabel=None, 
                 ylabel=None, 
                 legend=None, 
                 xlim=None,
                 ylim=None, 
                 xscale='linear', 
                 yscale='linear', 
                 fmts=None,
                 nrows=1, 
                 ncols=1, 
                 figsize=(10, 5)):

        if legend is None:
            legend = []
        pl = Plot()
        pl.use_svg_display()
        self.fig, self.axes = plt.subplots(nrows, ncols, figsize=figsize)
        if nrows * ncols == 1:
            self.axes = [self.axes, ]
        # Use a lambda to capture arguments
        self.config_axes = lambda: pl.set_axes(
            self.axes[0], xlabel, ylabel, xlim, ylim, xscale, yscale, legend)
        self.X, self.Y, self.fmts = None, None, fmts

    def add(self, x, y):
        """Add multiple data points into the figure."""
        if not hasattr(y, "__len__"):
            y = [y]
        n = len(y)
        if not hasattr(x, "__len__"):
            x = [x] * n
        if not self.X:
            self.X = [[] for _ in range(n)]
        if not self.Y:
            self.Y = [[] for _ in range(n)]
        if not self.fmts:
            self.fmts = ['-'] * n
        for i, (a, b) in enumerate(zip(x, y)):
            if a is not None and b is not None:
                self.X[i].append(a)
                self.Y[i].append(b)
        self.axes[0].cla()
        for x, y, fmt in zip(self.X, self.Y, self.fmts):
            self.axes[0].plot(x, y, fmt)
        self.config_axes()
        if display:
            display.display(self.fig)
            display.clear_output(wait=True)


class Timer:
    """Record multiple running times."""

    def __init__(self):
        self.times = []
        self.start()

    def start(self):
        # Start the timer
        self.tik = time.time()

    def stop(self):
        # Stop the timer and record the time in a list
        self.times.append(time.time() - self.tik)
        return self.times[-1]

    def avg(self):
        # Return the average time
        return sum(self.times) / len(self.times)

    def sum(self):
        # Return the sum of time
        return sum(self.times)

    def cumsum(self):
        # Return the accumulated times
        return np.array(self.times).cumsum().tolist()


class Accumulator:
    """Sum a list of numbers over time."""

    def __init__(self, n):
        self.data = [0.0] * n

    def add(self, *args):
        self.data = [a+float(b) for a, b in zip(self.data, args)]

    def reset(self):
        self.data = [0] * len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]