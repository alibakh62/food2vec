import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import numpy as np

from .dataprep_utils import W2VUtils
from .pretrained_embeddings_utils import LoadEmbeddings


def get_food_vectors(texts, embed_model, embed_size=100):
    '''
        Averages ingredients vectors to get food vector
    '''
    embeddings = LoadEmbeddings(embed_model)
    texts_, _ = W2VUtils.preprocess(texts)
    food_vec = []
    for item in texts_:
        cnt = 1
        foodvec = np.zeros(embed_size)  # embed size
        ingrds = item.split(',')
        for ingrd in ingrds:
            ingrd_ = ingrd.split(' ')
            for e in ingrd_:
                try:
                    foodvec += embeddings.get_embedding(e)
                    cnt += 1
                except KeyError:
                    pass
        foodvec /= cnt
        food_vec.append(foodvec)
    return food_vec


def calc_cosine_similarity(vec1, vec2):
    v1 = vec1[:,np.newaxis]
    v2 = vec2[:,np.newaxis]
    import math
    def dot_product(v1, v2):
        return sum(map(lambda x: x[0] * x[1], zip(v1, v2)))
    prod = dot_product(v1, v2)
    len1 = math.sqrt(dot_product(v1, v1))
    len2 = math.sqrt(dot_product(v2, v2))
    if (len1 == 0) | (len2 == 0):
        score = np.array([0])
    else:
        score = prod / (len1 * len2)
    return score


def get_food_index(labels, foodname):
    return labels.index(foodname)


def find_similar_food(query_token, food_vectors, food_labels, topn=5):
    try:
        query_index = get_food_index(food_labels, query_token)
    except KeyError:
        print("Food token not found!")
    query_vec = food_vectors[query_index]
    similarity_scores = {}
    for i, food in enumerate(food_vectors):
        # i_ = food_labels[i]
        if i != query_index:
            similarity_scores[i] = calc_cosine_similarity(query_vec, food)[0]
        else:
            similarity_scores[i] = 1
    # sort by similarity (ascending)
    sorted_scores = {k: v for k, v in sorted(similarity_scores.items(), key=lambda item: item[1])}
    idx_top = list(sorted_scores)[-topn:]
    idx_top_ = list(sorted_scores)[:topn]
    most_similar, least_similar = [], []
    for idx in idx_top:
        most_similar.append(food_labels[idx])
    for idx in idx_top_:
        least_similar.append(food_labels[idx])
    return sorted_scores, most_similar, least_similar


def find_similar_food_byVec(query_vec, food_vectors, food_labels, topn=5):
    similarity_scores = {}
    for i, food in enumerate(food_vectors):
        similarity_scores[i] = calc_cosine_similarity(query_vec, food)[0]
    # sort by similarity (ascending)
    sorted_scores = {k: v for k, v in sorted(similarity_scores.items(), key=lambda item: item[1])}
    idx_top = list(sorted_scores)[-topn:]
    idx_top_ = list(sorted_scores)[:topn]
    most_similar, least_similar = [], []
    for idx in idx_top:
        most_similar.append(food_labels[idx])
    for idx in idx_top_:
        least_similar.append(food_labels[idx])
    return sorted_scores, most_similar, least_similar