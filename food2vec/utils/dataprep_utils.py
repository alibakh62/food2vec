import os
import sys

sys.path.insert(0, '.')
sys.path.insert(0, '..')

import re
import numpy as np
import pandas as pd
import collections
import random
import time
import math
from glob import glob
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

from .mxnet_utils import RandomGenerator


import yaml
yml_path = glob(os.path.join('..', 'food2vec', '*.yml'))[0]
with open(yml_path, "rb") as yaml_to_read:
    yml = yaml.safe_load(yaml_to_read)


class Vocab:

    def __init__(self, sentences, min_freq=0, reserved_tokens=None):
        if reserved_tokens is None:
            reserved_tokens = []
        # Sort according to frequencies
        counter = W2VUtils.count_corpus(sentences)
        self.token_freqs = sorted(counter.items(), key=lambda x: x[0])
        self.token_freqs.sort(key=lambda x: x[1], reverse=True)
        self.unk, uniq_tokens = 0, ['<unk>'] + reserved_tokens
        uniq_tokens += [token for token, freq in self.token_freqs
                        if freq >= min_freq and token not in uniq_tokens]
        self.idx_to_token, self.token_to_idx = [], dict()
        for token in uniq_tokens:
            self.idx_to_token.append(token)
            self.token_to_idx[token] = len(self.idx_to_token) - 1

    def __len__(self):
        return len(self.idx_to_token)

    def __getitem__(self, tokens):
        if not isinstance(tokens, (list, tuple)):
            return self.token_to_idx.get(tokens, self.unk)
        return [self.__getitem__(token) for token in tokens]

    def to_tokens(self, indices):
        if not isinstance(indices, (list, tuple)):
            return self.idx_to_token[indices]
        return [self.idx_to_token[index] for index in indices]


class W2VUtils:


    @classmethod
    def remove_stopwords(cls, sentence):
        stop_words = set(stopwords.words('english'))
        return ' '.join(w for w in sentence.split() if w not in stop_words) 

    @classmethod
    def preprocess(cls, input_texts, filter=[], topn=100, remove_space=True):
        from collections import Counter
        texts = []
        for text in input_texts:
            tmp0 = (re.sub("[\(\[].*?[\)\]]", "", text) \
                      .replace(", ", ",") \
                      .replace(",  ", ",") \
                      .replace("  ,", ",") \
                      .replace(" ,", ",") \
                      .replace(" , ", ",") \
                      .lower())
            tmp1 = ','.join([cls.remove_stopwords(x) for x in tmp0.split(',')])  # remove stop words
            lemmatizer = WordNetLemmatizer()
            tmp2 = [lemmatizer.lemmatize(i) for i in tmp1.split(',')]  # lemmatize
            ingredients_cleaned = ','.join(tmp2)
            if remove_space:  #TODO: should we remove space?
                texts.append(re.sub('[^a-zA-Z,]+', '', ingredients_cleaned))  
            else:
                texts.append(re.sub('[^a-zA-Z, ]+', '', ingredients_cleaned))
        ingrd_ = [i for text in texts for i in text.split(',')]
        if filter:
            ingrd = [i for i in ingrd if i not in filter]
        else:
            ingrd = ingrd_
        c = Counter(ingrd).most_common(topn)
        topn_ingrd = [i[0] for i in c]
        return texts, topn_ingrd  

    @classmethod
    def get_sentences(cls, raw_texts):
        texts, _ = cls.preprocess(raw_texts)
        raw_text = ' \n'.join(texts)
        return [line.split() for line in raw_text.split('\n')]

    @staticmethod
    def count_corpus(sentences):
        # Flatten a list of token lists into a list of tokens
        tokens = [tk for line in sentences for tk in line]
        return collections.Counter(tokens)

    @classmethod
    def subsampling(cls, sentences, vocab):
        # Map low frequency words into <unk>
        sentences_ = [[vocab.idx_to_token[vocab[tk]] for tk in line]
                    for line in sentences]
        # Count the frequency for each word
        counter = cls.count_corpus(sentences_)
        num_tokens = sum(counter.values())
        # Return True if to keep this token during subsampling
        def keep(token):
            return(random.uniform(0, 1) <
                math.sqrt(1e-4 / counter[token] * num_tokens))
        # subsampling
        return [[tk for tk in line if keep(tk)] for line in sentences_]

    @classmethod
    def get_centers_and_contexts(cls, corpus, max_window_size):
        centers, contexts = [], []
        for line in corpus:
            # Each sentence needs at least 2 words to form a
            # "central target word - context word" pair
            if len(line) < 2:
                continue
            centers += line
            for i in range(len(line)):  # Context window centered at i
                window_size = random.randint(1, max_window_size)
                indices = list(range(max(0, i - window_size),
                                    min(len(line), i + 1 + window_size)))
                # Exclude the central target word from the context words
                indices.remove(i)
                contexts.append([line[idx] for idx in indices])
        return centers, contexts

    @classmethod
    def get_negatives(cls, all_contexts, sentences, num_noise_words):
        counter = cls.count_corpus(sentences)
        sampling_weights = [counter[i]**0.75 for i in range(len(counter))]
        all_negatives, generator = [], RandomGenerator(sampling_weights)
        for contexts in all_contexts:
            negatives = []
            while len(negatives) < len(contexts) * num_noise_words:
                neg = generator.draw()
                # Noise words cannot be context words
                if neg not in contexts:
                    negatives.append(neg)
            all_negatives.append(negatives)
        return all_negatives

    @staticmethod
    def compare_counts(token, all_sentences, subsampled_sentences):
        try:
            before = sum([line.count(token) for line in all_sentences])
            after = sum([line.count(token) for line in subsampled_sentences])
        except Exception as e:
            print(f"Need to subsample first! {e}")
        return f"# of token {token}: before={before}, after={after}"

    @staticmethod
    def get_corpus(vocab, sentences):
        return [vocab[line] for line in sentences]