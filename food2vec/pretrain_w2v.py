"""
    Training word vectors using Skip-gram Model
    -----
    We train a skip-gram model in this module. In forward calculation, 
    the input of the skip-gram model contains the central target word 
    index center and the concatenated context and noise word index 
    `contexts_and_negatives`. In which, the center variable has the shape
    `(batch size, 1)`, while the `contexts_and_negatives` variable has 
    the shape `(batch size, max_len)`. These two variables are first 
    transformed from word indexes to word vectors by the word embedding layer, 
    and then the output of shape `(batch size, 1, max_len)` is obtained by 
    minibatch multiplication. Each element in the output is the inner product 
    of the central target word vector and the context word vector or noise 
    word vector.
"""

import os
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')

import time
from glob import glob
from datetime import datetime

from utils.train_utils import TrainUtils
from .dataprep import DataLoader

from mxnet import autograd, gluon, np, npx 
from mxnet.gluon import nn
npx.set_np()

import yaml
yml_path = glob(os.path.join('..', 'food2vec', '*.yml'))[0]
with open(yml_path, "rb") as yaml_to_read:
    yml = yaml.safe_load(yaml_to_read)


def train(data_iter, vocab, save_path='../models'):
    npx.set_np()
    # loss function
    loss = gluon.loss.SigmoidBinaryCrossEntropyLoss()
    # initialize model parameters
    embed_size = yml['embed_size'] 
    net = nn.Sequential() 
    net.add(nn.Embedding(input_dim=len(vocab), output_dim=embed_size), 
            nn.Embedding(input_dim=len(vocab), output_dim=embed_size))
    TrainUtils.set_npx(npx)
    TrainUtils.train(net, data_iter, loss)
    version = datetime.now().strftime(format="%Y%m%d_%H%M%S")
    modelpath = os.path.join(save_path, 'embeddings_', version, '.params')
    print(f"saving the model to: {modelpath}")
    TrainUtils.save_model(modelpath)
    return TrainUtils.net
